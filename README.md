# Title: Expenses chart component

Tech stack: Vue.js & Vite

Deployed project: https://saad-shaikh-expenses-chart-component.netlify.app/

## Main tasks:
- Created an expenses bar chart where each individual bars show the correct amounts for each day when they are hovered on
- Made the bar for the day with the highest expenses highlighted in a different colour to the other bars
- Optimized the layout for different device screen sizes
- Created hover states for all interactive elements on the page
- Dynamically generated the bars based on the data provided in the local JSON file

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
